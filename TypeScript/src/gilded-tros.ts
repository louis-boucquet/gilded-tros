import { Item } from "./item";
import { clamp } from "./clamp";

const GOOD_WINE = "Good Wine";
const BACKSTAGE_PASSES_RE_FACTOR = "Backstage passes for Re:Factor";
const BACKSTAGE_PASSES_HAXX = "Backstage passes for HAXX";
const B_DAWG_KEYCHAIN = "B-DAWG Keychain";
const SMELLY_ITEMS = ["Duplicate Code", "Long Methods", "Ugly Variable Names"];

export function updateItems(items: Item[]) {
	items.forEach(updateItem);
}

export function updateItem(item: Item) {
	if (item.name === B_DAWG_KEYCHAIN) return;
	if (item.name === BACKSTAGE_PASSES_RE_FACTOR)
		return updateBackstagePassesReFactor(item);
	if (item.name === GOOD_WINE) return updateGoodWine(item);
	if (item.name === BACKSTAGE_PASSES_HAXX)
		return updateBackstagePassesHaxx(item);
	if (SMELLY_ITEMS.includes(item.name))
		return updateSmellyItem(item);

	updateRegularItem(item);
}

function updateBackstagePassesReFactor(item: Item) {
	if (item.sellIn <= 0) item.quality += 2;
	else if (item.sellIn <= 5) item.quality += 3;
	else if (item.sellIn <= 10) item.quality += 2;
	else item.quality += 1;

	item.sellIn = item.sellIn - 1;
	item.quality = clamp(item.quality, 0, 50);
}

function updateGoodWine(item: Item) {
	if (item.sellIn <= 0) item.quality += 2;
	else item.quality += 1;

	item.sellIn = item.sellIn - 1;
	item.quality = clamp(item.quality, 0, 50);
}

function updateBackstagePassesHaxx(item: Item) {
	if (item.sellIn > 0) item.quality += 1;

	item.sellIn = item.sellIn - 1;
	item.quality = clamp(item.quality, 0, 50);
}

function updateRegularItem(item: Item) {
	if (item.sellIn <= 0) item.quality = item.quality - 2;
	else item.quality = item.quality - 1;

	item.sellIn = item.sellIn - 1;
	item.quality = clamp(item.quality, 0, 50);
}

function updateSmellyItem(item: Item) {
	if (item.sellIn <= 0) item.quality = item.quality - 4;
	else item.quality = item.quality - 2;

	item.sellIn = item.sellIn - 1;
	item.quality = clamp(item.quality, 0, 50);
}
