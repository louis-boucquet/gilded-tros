import { Item } from "../src/item";
import { updateItem } from '../src/gilded-tros';

function testUpdate(item: Item, expected: Omit<Item, "update">) {
	updateItem(item);
	expect(item).toEqual(expected);
}

describe("regular item", () => {
	it("should update the item", () => {
		testUpdate(new Item("foo", 10, 10), {
			name: "foo",
			sellIn: 9,
			quality: 9,
		});
	});

	it("should not have negative quality", () => {
		testUpdate(new Item("foo", 0, 0), {
			name: "foo",
			sellIn: -1,
			quality: 0,
		});
	});

	it("should reduce quality by 2 when sellIn <= 0", () => {
		testUpdate(new Item("foo", 0, 10), {
			name: "foo",
			sellIn: -1,
			quality: 8,
		});
	});
});

describe("Backstage passes for Re:Factor", () => {
	it("should improve quality by 1 when above 10 days", () => {
		testUpdate(new Item("Backstage passes for Re:Factor", 11, 0), {
			name: "Backstage passes for Re:Factor",
			sellIn: 10,
			quality: 1,
		});
	});

	it("should improve quality by 1 when above 10 days", () => {
		testUpdate(new Item("Backstage passes for Re:Factor", 10, 0), {
			name: "Backstage passes for Re:Factor",
			sellIn: 9,
			quality: 2,
		});
	});

	it("should improve quality by 2 when above 5 days", () => {
		testUpdate(new Item("Backstage passes for Re:Factor", 6, 0), {
			name: "Backstage passes for Re:Factor",
			sellIn: 5,
			quality: 2,
		});
	});

	it("should improve quality by 3 when 5 days or bellow", () => {
		testUpdate(new Item("Backstage passes for Re:Factor", 5, 0), {
			name: "Backstage passes for Re:Factor",
			sellIn: 4,
			quality: 3,
		});
	});

	it("should improve quality by 3 when 1 day left", () => {
		testUpdate(new Item("Backstage passes for Re:Factor", 1, 0), {
			name: "Backstage passes for Re:Factor",
			sellIn: 0,
			quality: 3,
		});
	});

	it("should improve quality by 2 when 0 days left", () => {
		testUpdate(new Item("Backstage passes for Re:Factor", 0, 0), {
			name: "Backstage passes for Re:Factor",
			sellIn: -1,
			quality: 2,
		});
	});

	it("should not improve quality above 50", () => {
		testUpdate(new Item("Backstage passes for Re:Factor", 5, 49), {
			name: "Backstage passes for Re:Factor",
			sellIn: 4,
			quality: 50,
		});
	});
});

describe("Good Wine", () => {
	it("should improve quality", () => {
		testUpdate(new Item("Good Wine", 10, 10), {
			name: "Good Wine",
			sellIn: 9,
			quality: 11,
		});
	});

	it("should improve quality by 2 when sellIn <= 0", () => {
		testUpdate(new Item("Good Wine", 0, 0), {
			name: "Good Wine",
			sellIn: -1,
			quality: 2,
		});
	});

	it("should improve quality by 2 when sellIn < 0", () => {
		testUpdate(new Item("Good Wine", -1, 0), {
			name: "Good Wine",
			sellIn: -2,
			quality: 2,
		});
	});

	it("should not improve quality above 50", () => {
		testUpdate(new Item("Good Wine", -1, 50), {
			name: "Good Wine",
			sellIn: -2,
			quality: 50,
		});
	});
});

describe("Backstage passes for HAXX", () => {
	it("should improve quality", () => {
		testUpdate(new Item("Backstage passes for HAXX", 10, 10), {
			name: "Backstage passes for HAXX",
			sellIn: 9,
			quality: 11,
		});
	});
});

describe("B-DAWG Keychain", () => {
	it("should not update the item", () => {
		testUpdate(new Item("B-DAWG Keychain", 0, 0), {
			name: "B-DAWG Keychain",
			sellIn: 0,
			quality: 0,
		});

		testUpdate(new Item("B-DAWG Keychain", 10, 10), {
			name: "B-DAWG Keychain",
			sellIn: 10,
			quality: 10,
		});
	});

	it("should allow the item to have quality above 50", () => {
		testUpdate(new Item("B-DAWG Keychain", 0, 80), {
			name: "B-DAWG Keychain",
			sellIn: 0,
			quality: 80,
		});
	});
});

describe.each(["Duplicate Code", "Long Methods", "Ugly Variable Names"])('Smelly items: %s', (name) => {
	it("should update the item", () => {
		testUpdate(new Item(name, 10, 10), {
			name: name,
			sellIn: 9,
			quality: 8,
		});
	});

	it("should not have negative quality", () => {
		testUpdate(new Item(name, 0, 1), {
			name: name,
			sellIn: -1,
			quality: 0,
		});
	});

	it("should reduce quality by 2 when sellIn <= 0", () => {
		testUpdate(new Item(name, 0, 10), {
			name: name,
			sellIn: -1,
			quality: 6,
		});
	});
})
